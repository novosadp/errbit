# -*- encoding : utf-8 -*-
ActionMailer::Base.delivery_method = :smtp
if Rails.env == "production"
	ActionMailer::Base.smtp_settings = {
    :address              => Rails.application.secrets.email_smtp,
    :port                 => Rails.application.secrets.email_port,
    :domain               => Rails.application.secrets.email_domain,
    :user_name            => Rails.application.secrets.email,
    :password             => Rails.application.secrets.email_password,
    :authentication       => "plain",
    :enable_starttls_auto => false,
    :ssl                  => true
  }
end

#   ActionMailer::Base.default_url_options = { host:'localhost', port: '3000' }
#   ActionMailer::Base.perform_deliveries = true
#   ActionMailer::Base.raise_delivery_errors = true
#   # ActionMailer::Base.default_url_options = { host:'admin@', port: '3000' }
#   ActionMailer::Base.default :charset => "utf-8"
#   ActionMailer::Base.smtp_settings = {
#       :address => '127.0.0.1',
#       :port => 25,
#       # :user_name            => EMAIL_CONFIG[:username],
#       # :password             => EMAIL_CONFIG[:password],
#       # :authentication => :plain,
#       :enable_starttls_auto => false
#   }
end


